defmodule Tm1.Repo do
  use Ecto.Repo,
    otp_app: :tm1,
    adapter: Ecto.Adapters.Postgres
end
