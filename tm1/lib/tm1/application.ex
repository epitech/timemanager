defmodule Tm1.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Tm1.Repo,
      # Start the Telemetry supervisor
      Tm1Web.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Tm1.PubSub},
      # Start the Endpoint (http/https)
      Tm1Web.Endpoint
      # Start a worker by calling: Tm1.Worker.start_link(arg)
      # {Tm1.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Tm1.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Tm1Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
