defmodule Tm1.Schema.UserTeam do
  use Ecto.Schema
  import Ecto.Changeset

  @already_exists "ALREADY_EXISTS"

  @primary_key false
  schema "user_team" do
    belongs_to(:user, User, primary_key: true)
    belongs_to(:team, Team, primary_key: true)

    timestamps()
  end

  @doc false
  def changeset(user_team, attrs) do
    user_team
    |> cast(attrs, [:user_id,:team_id])
  end
end
