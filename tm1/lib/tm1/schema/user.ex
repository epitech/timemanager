defmodule Tm1.Schema.User do
  use Ecto.Schema
  import Ecto.Changeset

  import Pow.Ecto.Schema.Changeset, only: [new_password_changeset: 3]

  #clsimport Comeonin.Bcrypt, only: [hashpwsalt: 1]

  use Pow.Ecto.Schema,
      user_id_field: :email,
      password_hash_methods: {&Pow.Ecto.Schema.Password.pbkdf2_hash/1,
        &Pow.Ecto.Schema.Password.pbkdf2_verify/2},
      password_min_length: 3,
      password_max_length: 4096

  schema "users" do
    field :email, :string
    field :username, :string
    field :password, :string
    field(:roles, {:array, :string}, default: ["employe"])

    #virtual
    field :password_, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    has_many :working_times, Tm1.Schema.WorkingTime, on_delete: :delete_all
    has_many :clocks, Tm1.Schema.Clock, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :roles, :password_, :password_confirmation])
    |> validate_required([:username, :email, :roles, :password_, :password_confirmation])
    |> validate_format(:email, ~r/@/)
    |> validate_confirmation(:password_)
    |> unique_constraint(:email)
    |> put_password_hash
    #|> verify_password(attrs, [:username, :email, :roles, :password])
    #|> put_change(:password, hashed_password(attrs["password"]))
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password_: pass}}
      ->
        put_change(changeset, :password, Comeonin.Pbkdf2.hashpwsalt(pass))
      _ ->
        changeset
    end
  end
end
