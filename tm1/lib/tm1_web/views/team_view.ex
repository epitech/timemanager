defmodule Tm1Web.TeamView do
  use Tm1Web, :view
  alias Tm1Web.TeamView
  alias Tm1Web.UserView

  def render("index.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "team.json")}
  end

  def render("show.json", %{team: team}) do
    %{data: render_one(team, TeamView, "team.json")}
  end

  def render("team.json", %{team: team}) do
    %{id: team.id,
      name: team.name}
  end

  def render("index_team_user.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

end
