defmodule Tm1Web.TeamController do
  use Tm1Web, :controller

  alias Tm1.Schema
  alias Tm1.Schema.Team
  alias Tm1.Schema.UserTeam

  action_fallback Tm1Web.FallbackController

  def index(conn, _params) do
    teams = Schema.list_teams()
    render(conn, "index.json", teams: teams)
  end

  def create(conn, %{"team" => team_params}) do
    with {:ok, %Team{} = team} <- Schema.create_team(team_params) do
      conn
      |> put_status(:created)
      |> render("show.json", team: team)
    end
  end

  def addUserInTeam(conn, %{"user_team" => team_user_params}) do
    IO.inspect(team_user_params)
    with {:ok, %UserTeam{} = user_team} <- Schema.add_user_team(team_user_params) do
      conn
      |> put_status(:created)
      |> redirect(to: Routes.team_path(conn, :index, team_user_params))
    end
  end

  def getEmployeOnTeam(conn, %{"id" => id_team}) do
    emplyesTeam = Schema.get_employe(id_team)
    render(conn, "index_team_user.json", users: emplyesTeam)
  end

  def show(conn, %{"id" => id}) do
    team = Schema.get_team!(id)
    render(conn, "show.json", team: team)
  end

  def update(conn, %{"id" => id, "team" => team_params}) do
    team = Schema.get_team!(id)

    with {:ok, %Team{} = team} <- Schema.update_team(team, team_params) do
      render(conn, "show.json", team: team)
    end
  end

  def delete(conn, %{"id" => id}) do
    team = Schema.get_team!(id)

    with {:ok, %Team{}} <- Schema.delete_team(team) do
      send_resp(conn, :no_content, "")
    end
  end
end
