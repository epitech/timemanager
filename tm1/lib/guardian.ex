defmodule Tm1.Guardian do
  use Guardian, otp_app: :tm1

  def subject_for_token(user, _claims) do
    sub = to_string(user.id)
    roles = to_string(user.roles)
    {:ok,[user.id,user.roles]}
  end

  def subject_for_token(_, _) do
    {:error, :reason_for_error}
  end

  def resource_from_claims(claims) do
    id = claims["sub"]
    resource = Tm1.Accounts.get_user!(id)
    {:ok,  resource}
  end

  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end