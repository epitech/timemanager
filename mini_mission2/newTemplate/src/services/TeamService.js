import http from "../http-common";

class UserService {
    getAll() {
        return http.get("/teams");
    }

    getOne(user_id) {
        return http.get(`/users/${user_id}`);
    }

    getByUsernameEmail(email, username) {
        return http.get(`/users?email=${email}&username=${username}`);
    }

    create(data) {
        return http.post("/teams", data);
    }

    addUserInTeam(data) {
        return http.post("/teamUser", data);
    }


    update(data, id) {
        return http.put(`/users/${id}`, data);
    }

    login(data) {
        return http.post("/login", data);
    }

    edit(user_id, data) {
        return http.put(`/users/${user_id}`, data);
    }

    delete(user_id) {
        return http.delete(`/users/${user_id}`);
    }
}

export default new UserService();