import http from "../http-common";

class ClockService {
    get(user_id) {
        return http.get(`/clocks/${user_id}`);
    }

    create(data, user_id) {
        return http.post(`/clocks/${user_id}`, data);
    }
}

export default new ClockService();