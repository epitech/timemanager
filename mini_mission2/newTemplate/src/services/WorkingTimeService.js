import http from "../http-common";

class WorkingTimeService {
    getAll(id) {
        return http.get(`/workingtimes/${id}`);
    }

    getLast(id) {
        return http.get(`/workingtime/${id}`);
    }

    getAllWithDate(id, start, end) {
        return http.get(`/workingtimes/${id}?start=${start}&end=${end}`);
    }

    create(data, userID) {
        return http.post(`/workingtimes/${userID}`, data);
    }

    update(data, workingTimeID) {
        return http.put(`/workingtimes/${workingTimeID}`, data);
    }

    delete(id) {
        return http.delete(`/workingtimes/${id}`);
    }
}

export default new WorkingTimeService();