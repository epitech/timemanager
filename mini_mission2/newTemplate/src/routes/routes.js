import DashboardLayout from '@/views/Layout/DashboardLayout.vue';
import AuthLayout from '@/views/Pages/AuthLayout.vue';

import NotFound from '@/views/NotFoundPage.vue';

const routes = [{
        path: '/',
        redirect: 'dashboard',
        component: DashboardLayout,
        children: [{
                path: '/dashboard',
                name: 'dashboard',
                // route level code-splitting
                // this generates a separate chunk (about.[hash].js) for this route
                // which is lazy-loaded when the route is visited.
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/Dashboard.vue')
            },
            {
                path: '/icons',
                name: 'icons',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/Icons.vue')
            },
            {
                path: '/profile',
                name: 'profile',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/Pages/UserProfile.vue')
            },
            {
                path: '/maps',
                name: 'maps',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/GoogleMaps.vue')
            },
            {
                path: '/tables',
                name: 'tables',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/UserComponent.vue')
            },
            {
                path: '/create-employe',
                name: 'Create Employe',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/UserCreateComponent.vue')
            },
            {
                path: '/edit-employe/:id',
                name: 'Edit Employe',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/UserEditComponent.vue')
            },
            {
                path: '/create-team',
                name: 'Create Team',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/TeamCreateComponent.vue')
            },
            {
                path: '/workingTimes/:id',
                name: 'WorkingTimes',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/WorkingTimesComponent.vue')
            },
            {
                path: '/teams',
                name: 'Teams',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/TeamComponent.vue')
            },
            {
                path: '/pointing',
                name: 'Pointing',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/PointingComponent.vue')
            },
            {
                path: '/charts/:id',
                name: 'Teams',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/ChartComponent.vue')
            },
        ]
    },
    {
        path: '/',
        redirect: 'login',
        component: AuthLayout,
        children: [{
                path: '/login',
                name: 'login',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/Pages/Login.vue')
            },
            {
                path: '/register',
                name: 'register',
                component: () =>
                    import ( /* webpackChunkName: "demo" */ '../views/Pages/Register.vue')
            },
            { path: '*', component: NotFound }
        ]
    }
];

export default routes;