import http from "../http-common";

class UserService {
    getAll() {
        return http.get("/users");
    }

    getOne(user_id) {
        return http.get(`/users/${user_id}`);
    }

    getByUsernameEmail(email, username) {
        return http.get(`/users?email=${email}&username=${username}`);
    }

    create(data) {
        return http.post("/users", data);
    }

    login(data) {
        return http.post("/login", data);
    }

    edit(user_id, data) {
        return http.put(`/users/${user_id}`, data);
    }

    delete(user_id) {
        return http.delete(`/users/${user_id}`);
    }
}

export default new UserService();