import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        // {
        //   path: "/",
        //   alias: "/donut",
        //   name: "donut",
        //   component: () => import("./components/DonutChart")
        // },
        {
            path: "/user",
            name: "user",
            component: () =>
                import ("./components/UserComponent")
        },
        {
            path: "/user-edit/:id",
            name: "user-edit",
            component: () =>
                import ("./components/UserEditComponent")
        },
        {
            path: "/user-new",
            name: "user-edit",
            component: () =>
                import ("./components/UserCreateComponent")
        },
        {
            path: "/user-login",
            name: "user-edit",
            component: () =>
                import ("./components/UserLogIn")
        },
        {
            path: "/workingTimes/:id",
            name: "user-edit",
            component: () =>
                import ("./components/WorkingTimesComponent")
        },
        {
            path: "/charts/:id",
            name: "charts",
            component: () =>
                import ("./components/ChartComponent")
        },
        // {
        //     path: "/pointage",
        //     name: "pointage",
        //     component: () =>
        //         import ("./components/WorkingTimeComponent")
        // },
        // {
        //   path: "/",
        //   name: "line",
        //   component: () => import("./components/LineChart")
        // },
        // {
        //   path: "/bar",
        //   alias: "/bar",
        //   name: "bar",
        //   component: () => import("./components/BarChart")
        // }
    ]
});