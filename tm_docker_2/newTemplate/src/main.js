/*!

=========================================================
* BootstrapVue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/bootstrap-vue-argon-dashboard
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue';
import DashboardPlugin from './plugins/dashboard-plugin';
import App from './App.vue';
import jQuery from 'jQuery'
global.jQuery = jQuery

import LineChart from './views/LineChart.vue'
import BarChart from './views/BarChart.vue'
import DonutChart from './views/DonutChart.vue'
import UserComponent from './views/UserComponent.vue'
import AreaChart from './views/AreaChart.vue'

import Raphael from 'raphael/raphael'
global.Raphael = Raphael

// router setup
import router from './routes/router';
// plugin setup
Vue.use(DashboardPlugin);

Vue.component('app-linechart', LineChart)
Vue.component('app-barchart', BarChart)
Vue.component('app-donutchart', DonutChart)
Vue.component('app-user', UserComponent)
Vue.component('app-areachart', AreaChart)
    /* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App),
    router
});