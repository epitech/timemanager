defmodule Tm1.Repo.Migrations.AddColumnsUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :password, :string
      add :roles, {:array, :string}, default: ["employe"]
    end
  end
end
