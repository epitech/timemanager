defmodule Tm1.Schema.User do
  use Ecto.Schema
  import Ecto.Changeset

  import Pow.Ecto.Schema.Changeset, only: [new_password_changeset: 3]

  use Pow.Ecto.Schema,
      user_id_field: :email,
      password_hash_methods: {&Pow.Ecto.Schema.Password.pbkdf2_hash/1,
        &Pow.Ecto.Schema.Password.pbkdf2_verify/2},
      password_min_length: 3,
      password_max_length: 4096

  schema "users" do
    field :email, :string
    field :username, :string
    field :password, :string
    field(:roles, {:array, :string}, default: ["employe"])
    has_many :working_times, Tm1.Schema.WorkingTime, on_delete: :delete_all
    has_many :clocks, Tm1.Schema.Clock, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :roles, :password])
    |> validate_required([:username, :email])
    |> validate_format(:email, ~r/@/)
    #|> verify_password(attrs, [:username, :email, :roles, :password])
    |> put_change(:password, hashed_password(attrs["password"]))
  end

  defp hashed_password(password) do
    case password do
      nil -> ""
      _ -> Comeonin.Pbkdf2.hashpwsalt(password)
    end
  end
end
