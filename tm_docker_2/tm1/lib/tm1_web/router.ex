defmodule Tm1Web.Router do
  use Tm1Web, :router

  pipeline :browser do
    plug CORSPlug, origin: "*"
    plug :accepts, ["html"]
    #plug :fetch_session
    #plug :fetch_flash
    #plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
    plug Tm1.Auth.AuthFlow, otp_app: :tm1
  end

  scope "/", Tm1Web do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", Tm1Web do
    pipe_through :api
    options "/users", UserController, :options
    get "/users", UserController, :index
    post "/users", UserController, :create
    get "/users/:id", UserController, :show
    put "/users/:id", UserController, :update
    delete "/users/:id", UserController, :delete

    get "/workingtimes/:userID/:workingtimeID", WorkingTimeController, :show
    get "/workingtimes/:userID", WorkingTimeController, :index
    post "/workingtimes/:userID", WorkingTimeController, :create
    put "/workingtimes/:id", WorkingTimeController, :update
    delete "/workingtimes/:id", WorkingTimeController, :delete

    get "/clocks/:userID", ClockController, :index
    post "/clocks/:userID", ClockController, :create

    post "/teams", TeamController, :create
    get "/teams", TeamController, :index
    delete "/teams/:id", TeamController, :delete
    put "/teams/:id", TeamController, :update
    post "/teamUser", TeamController, :addUserInTeam
    get "/teamUser/:id", TeamController, :getEmployeOnTeam

    post "/login", UserController, :login

  end


  # Other scopes may use custom stacks.
  # scope "/api", Tm1Web do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: Tm1Web.Telemetry
    end
  end
end
