defmodule Tm1Web.ClockController do
  use Tm1Web, :controller

  alias Tm1.Schema
  alias Tm1.Schema.Clock
  alias Tm1.Repo
  alias Tm1.Schema.User

  action_fallback Tm1Web.FallbackController

  def index(conn, _params) do
    userID = _params["userID"]
    clocks = Schema.get_clocks_by_user!(userID)
    render(conn, "index.json", clocks: clocks)
  end

#  def create(conn, %{"clock" => clock_params}) do
  def create(conn, %{"clock" => clock_params, "userID" => user}) do

      #    with {:ok, %Clock{} = clock} <- Schema.create_clock(clock_params) do
    user = Repo.get(User, user)
    time = NaiveDateTime.from_iso8601!(clock_params["time"])
    status = clock_params["status"]
    clock_changeset = Ecto.build_assoc(user, :clocks, %{time: time, status: status})
    Repo.insert(clock_changeset)

      conn
        |> put_status(:created)
        |> render("show.json", clock: clock_changeset)
  end

  def show(conn, %{"id" => id}) do
    clock = Schema.get_clock!(id)
    render(conn, "show.json", clock: clock)
  end

  def update(conn, %{"id" => id, "clock" => clock_params}) do
    clock = Schema.get_clock!(id)

    with {:ok, %Clock{} = clock} <- Schema.update_clock(clock, clock_params) do
      render(conn, "show.json", clock: clock)
    end
  end

  def delete(conn, %{"id" => id}) do
    clock = Schema.get_clock!(id)

    with {:ok, %Clock{}} <- Schema.delete_clock(clock) do
      send_resp(conn, :no_content, "")
    end
  end
end
