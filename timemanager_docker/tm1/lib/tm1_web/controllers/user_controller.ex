defmodule Tm1Web.UserController do
  use Tm1Web, :controller

  alias Tm1.Schema
  alias Tm1.Schema.User

  action_fallback Tm1Web.FallbackController

  def index(conn, _params) do
    if _params["email"] && _params["username"] do
      email = _params["email"]
      username = _params["username"]
      users = Schema.get_user_by_email_username!(email,username)
      render(conn, "show.json", user: users)
    end
    users = Schema.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Schema.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Schema.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Schema.get_user!(id)

    with {:ok, %User{} = user} <- Schema.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Schema.get_user!(id)

    with {:ok, %User{}} <- Schema.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
