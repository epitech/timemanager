defmodule Tm1Web.WorkingTimeController do
  use Tm1Web, :controller


  alias Tm1.Schema
  alias Tm1.Schema.WorkingTime
  alias Tm1.Repo
  alias Tm1.Schema.User


  action_fallback Tm1Web.FallbackController

  def index(conn, _params) do
    startDate = _params["start"]
    endDate = _params["end"]
    user = _params["userID"]
    workingtimes = Schema.get_all_workingtimes_by_user!(user, startDate, endDate)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def create(conn, %{"working_time" => working_time_params, "userID" => user}) do
    user = Repo.get(User, user)
    endDate = NaiveDateTime.from_iso8601!(working_time_params["end"])
    startDate = NaiveDateTime.from_iso8601!(working_time_params["start"])
    working_time_changeset = Ecto.build_assoc(user, :working_times, %{end: endDate,start: startDate })
    Repo.insert(working_time_changeset)

    #conn
    #|> put_status(:created)
    #|> redirect(to: Routes.user_path(conn, :show, user))
    conn
    |> put_status(:created)
    |> render("show.json", working_time: working_time_changeset)
  end

  def show(conn, %{"userID" => userID, "workingtimeID" => workingtimeID}) do
    workingtimes = Schema.get_one_workingtimes_by_user!(userID, workingtimeID)
    render(conn, "show.json", working_time: workingtimes)
  end

  def update(conn, %{"id" => id, "working_time" => working_time_params}) do
    working_time = Schema.get_working_time!(id)

    with {:ok, %WorkingTime{} = working_time} <- Schema.update_working_time(working_time, working_time_params) do
      render(conn, "show.json", working_time: working_time)
    end
  end

  def delete(conn, %{"id" => id}) do
    working_time = Schema.get_working_time!(id)

    with {:ok, %WorkingTime{}} <- Schema.delete_working_time(working_time) do
      send_resp(conn, :no_content, "")
    end
  end
end
