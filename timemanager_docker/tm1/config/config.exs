# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :tm1,
  ecto_repos: [Tm1.Repo]

# Configures the endpoint
config :tm1, Tm1Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "UKLgk4fC7jHyMSx/BTJFQslgzN2pdvwB+3feachoXEAg/52KCsjQMDXnhvx4mVw3",
  render_errors: [view: Tm1Web.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Tm1.PubSub,
  live_view: [signing_salt: "nIaJXq8o"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
