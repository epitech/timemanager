import http from "../http-common";

class ClockService {
    getUserClock(user_id) {
        return http.get(`/clocks/${user_id}`);
    }

    createUserClock(user_id, data) {
        return http.post(`/clocks/${user_id}`, data);
    }
}

export default new ClockService();