import http from "../http-common";

class WorkingTimeService {
    getAll() {
        return http.get("/workingtimes");
    }

    create(data) {
        return http.post("/workingtimes", data);
    }

    delete(id) {
        return http.delete(`/workingtimes/${id}`);
    }
}

export default new WorkingTimeService();