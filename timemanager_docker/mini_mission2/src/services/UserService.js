import http from "../http-common";

class UserService {
  getAll() {
    return http.get("/users");
  }

  create(data) {
    return http.post("/users", data);
  }

  delete(user_id) {
    return http.delete(`/users/${user_id}`);
  }
}

export default new UserService();