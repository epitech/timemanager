import Vue from 'vue'
import App from './App.vue'
import router from './router'
import LineChart from './components/LineChart.vue'
import BarChart from './components/BarChart.vue'
import DonutChart from './components/DonutChart.vue'
import UserComponent from './components/UserComponent.vue'
import AreaChart from './components/AreaChart.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
global.BootstrapVue = BootstrapVue
global.IconsPlugin = IconsPlugin
import jQuery from 'jQuery'
global.jQuery = jQuery

import Raphael from 'raphael/raphael'
global.Raphael = Raphael

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap/dist/bootstrap-vue.css'

Vue.component('app-linechart', LineChart)
Vue.component('app-barchart', BarChart)
Vue.component('app-donutchart', DonutChart)
Vue.component('app-user', UserComponent)
Vue.component('app-areachart', AreaChart)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
